<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function acf_events_manage_columns( $column, $post_id ) {
    global $post;

    if ( $column == 'event_starting_date' || $column == 'event_ending_date' ) {
        $date = get_post_meta( $post_id, $column, true );
    }

    echo __( acf_events_date_format($date) );
}
add_action( 'manage_event_posts_custom_column', 'acf_events_manage_columns', 10, 2 );

function acf_events_custom_post_order( $query ) {

    if ( ! is_admin() ) return false;

    global $pagenow;

    remove_all_filters('posts_orderby'); 
    $post_type = $query->get('post_type');
    
    if ( $pagenow == 'edit.php' && $post_type == 'event' ) {

        $orderby    = isset( $_GET['orderby'] ) ? $_GET['orderby'] : 'event_starting_date';
        $sort       = isset( $_GET['order'] ) ? $_GET['order'] : 'desc';

        if ( $orderby == 'event_starting_date' || $orderby == 'event_ending_date' ) {
            $query->set( 'orderby', 'meta_value_num' );
            $query->set( 'meta_key', $orderby );
            $query->set( 'order', $sort );
        }        
    }
}
add_action( 'pre_get_posts', 'acf_events_custom_post_order' );

?>