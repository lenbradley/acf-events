<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_shortcode( 'events_calendar', 'get_events_calendar' );

?>