<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function initialize_acf_events_plugin() {

    if ( is_admin() ) {
        // Enqueue scripts and stylesheets
        wp_enqueue_script( 'acf-events', plugins_url( ACF_EVENTS_PLUGIN ) . '/js/acf-events.js', 'jquery' );
        wp_enqueue_style( 'acf-events', plugins_url( ACF_EVENTS_PLUGIN ) . '/css/acf-events.css' );
    }

    // Register the event post type
    register_post_type(
        'event', array(
            'label' => __( 'Events', 'acf-events-plugin' ),
            'description' => __( 'Event post type for ACF-Events plugin', 'acf-events-plugin' ),
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'capability_type' => 'post',
            'map_meta_cap' => true,
            'hierarchical' => false,
            'rewrite' => array(
                'slug' => 'event',
                'with_front' => false
            ),
            'query_var' => true,
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'trackbacks',
                'custom-fields',
                'comments',
                'revisions',
                'thumbnail',
                'author',
                'page-attributes',
                'post-formats'
            ),
            'labels' => array (
                'name' => __( 'Events', 'acf-events-plugin' ),
                'singular_name' => __( 'Event', 'acf-events-plugin' ),
                'menu_name' => __( 'Events', 'acf-events-plugin' ),
                'add_new' => __( 'Add Event', 'acf-events-plugin' ),
                'add_new_item' => __( 'Add New Event', 'acf-events-plugin' ),
                'edit' => __( 'Edit', 'acf-events-plugin' ),
                'edit_item' => __( 'Edit Event', 'acf-events-plugin' ),
                'new_item' => __( 'New Event', 'acf-events-plugin' ),
                'view' => __( 'View Event', 'acf-events-plugin' ),
                'view_item' => __( 'View Event', 'acf-events-plugin' ),
                'search_items' => __( 'Search Events', 'acf-events-plugin' ),
                'not_found' => __( 'No Events Found', 'acf-events-plugin' ),
                'not_found_in_trash' => __( 'No Events Found in Trash', 'acf-events-plugin' ),
                'parent' => __( 'Parent Event', 'acf-events-plugin' )
            ),
            'menu_icon' => 'dashicons-calendar'
        )
    );

    // Register default taxonomy for events
    register_taxonomy(
        'event-type',
        array (
            0 => 'event',
        ),
        array(
            'hierarchical' => true,
            'label' => 'Event Types',
            'show_ui' => true,
            'query_var' => true,
            'show_admin_column' => false,
            'labels' => array (
                'search_items' => 'Event Types',
                'popular_items' => 'Popular Event Types',
                'all_items' => 'All Event Types',
                'edit_item' => 'Edit Event Type',
                'update_item' => 'Update Event Type',
                'add_new_item' => 'Add Event Type',
                'new_item_name' => 'New Event Type',
                'separate_items_with_commas' => 'Separate with commas',
                'add_or_remove_items' => 'Add or Remove Event Types',
                'choose_from_most_used' => 'Most Used Event Types',
            )
        )
    ); 

    // Register custom fields
    if ( function_exists('register_field_group') ) {
        register_field_group( array (
            'id' => 'acf_event-settings',
            'title' => 'Event Settings',
            'fields' => array (
                array (
                    'key' => 'field_53cd79e90b386',
                    'label' => 'Starting Date',
                    'name' => 'event_starting_date',
                    'type' => 'date_picker',
                    'date_format' => 'yymmdd',
                    'display_format' => 'mm/dd/yy',
                    'first_day' => 1,
                ),
                array (
                    'key' => 'field_53cd7a190b387',
                    'label' => 'Ending Date',
                    'name' => 'event_ending_date',
                    'type' => 'date_picker',
                    'date_format' => 'yymmdd',
                    'display_format' => 'mm/dd/yy',
                    'first_day' => 1,
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'event',
                        'order_no' => 0,
                        'group_no' => 0,
                    ),
                ),
            ),
            'options' => array (
                'position' => 'normal',
                'layout' => 'default',
                'hide_on_screen' => array (
                ),
            ),
            'menu_order' => 0,
        ));
    }
}
add_action( 'init', 'initialize_acf_events_plugin' );

?>