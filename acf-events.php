<?php
/*
Plugin Name: ACF Events
Plugin URI: http://www.ideamktg.com
Description: An events plugin utilizing the amazing power of Elliot Condon's <a href="http://www.advancedcustomfields.com/">Advanced Custom Fields</a> plugin (required).
Author: Idea Marketing Group
Version: 1.0.1
Author URI: http://www.ideamktg.com
Text Domain: acf-events-plugin
License: GPLv2
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function acf_events_init() {
    if ( ! class_exists('acf') ) {
        define( 'ACF_EVENTS_ACTIVE', false );

        function my_admin_notice() {
            ?>
            <div class="error">
                <p><?php _e( 'Advanced Custom Fields plugin must be installed and active to use the ACF-Events plugin!' ); ?></p>
            </div>
            <?php
        }
        add_action( 'admin_notices', 'my_admin_notice' );
    } else {
        define( 'ACF_EVENTS_ACTIVE', true );
        define( 'ACF_EVENTS_VERSION', '1.0.1' );
        define( 'ACF_EVENTS_PLUGIN', 'acf-events' );
        define( 'ACF_EVENTS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

        require ACF_EVENTS_PLUGIN_DIR . '/inc/init.php';
        require ACF_EVENTS_PLUGIN_DIR . '/inc/functions.php';
        require ACF_EVENTS_PLUGIN_DIR . '/inc/actions.php';
        require ACF_EVENTS_PLUGIN_DIR . '/inc/filters.php';
        require ACF_EVENTS_PLUGIN_DIR . '/inc/shortcodes.php';
        require ACF_EVENTS_PLUGIN_DIR . '/inc/widgets.php';
    }
}
add_action( 'plugins_loaded', 'acf_events_init' );

?>