<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Upcoming_Events_Widget extends WP_Widget {

    public function __construct() {
        parent::__construct( 'upcoming_events_widget', 'Upcoming Events', array( 'description' => 'Show the most recent upcoming events' ) );
    }

    public function widget( $args, $instance ) {

        $html   = '';
        $today  = date('Ymd');

        remove_all_filters('posts_orderby');

        $events = new WP_Query( array(
            'post_type'         => 'event',
            'posts_per_page'    => 5,
            'meta_key'          => 'event_ending_date',
            'orderby'           => 'meta_value_num',
            'order'             => 'ASC',
            'meta_query' => array(
                array(                    
                    'key'       => 'event_ending_date',
                    'value'     => $today,
                    'compare'   => '>',
                    'type'      => 'numeric'
                    )
                )
        ));

        if ( ! empty( $events->posts ) ) {
            foreach ( $events->posts as $event ) {
                $starting   = get_field( 'event_starting_date', $event->ID );
                $ending     = get_field( 'event_ending_date', $event->ID );

                $html .= '
                    <li>
                        <a href="' . get_permalink( $event->ID ) . '">
                            <strong>' . $event->post_title . '</strong><br>
                            ' . acf_events_date_format( $starting, $ending ) . '
                        </a>
                    </li>';
            }
        }

        if ( $html != '' ) {
            $html = '<ul>' . $html . '</ul>';
        }

        echo $args['before_widget'];
        echo $args['before_title'] . 'Upcoming Events' . $args['after_title'];
        echo $html;
        echo $args['after_widget'];

    }
}
add_action( 'widgets_init', function() {
     register_widget( 'Upcoming_Events_Widget' );
});

?>