<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function get_events( $args = array() ) {

    if ( ! is_array( $args ) ) {
        $args = array();
    }

    $output     = array();    
    $today      = date('Ymd');

    $defaults = array(
        'type'      => 'upcoming',
        'limit'     => get_option('posts_per_page'),
        'page'      => 1,
        'from'      => $today,
        'to'        => $today,
        'date'      => $today,
        'order'     => 'ASC',
        'orderby'   => 'meta_value_num',
        'meta_key'  => 'event_starting_date'
    );
    $args = array_merge( $defaults, $args );

    switch( $args['type'] ) {
        case 'upcoming' : 
            $meta_query = array(
                array(
                    'key'       => 'event_ending_date',
                    'value'     => $today,
                    'compare'   => '>=',
                    'type'      => 'NUMERIC'
                )
            );            
            break;
        case 'past' : 
            $meta_query = array(
                array(
                    'key'       => 'event_ending_date',
                    'value'     => $today,
                    'compare'   => '<',
                    'type'      => 'NUMERIC'
                )
            );

            $args['order'] = 'DESC';
            break;
        case 'current' :            
            $meta_query = array(
                'relation' => 'AND',
                array(
                    'key'       => 'event_starting_date',
                    'value'     => $today,
                    'compare'   => '<=',
                    'type'      => 'NUMERIC'
                ),               
                array(
                    'key'       => 'event_ending_date',
                    'value'     => $today,
                    'compare'   => '>=',
                    'type'      => 'NUMERIC'
                )
            );
            break; 
        case 'range' :            
            $meta_query = array(
                'relation' => 'AND',
                array(
                    'key'       => 'event_starting_date',
                    'value'     => $args['from'],
                    'compare'   => '>=',
                    'type'      => 'NUMERIC'
                ),
                array(
                    'key'       => 'event_ending_date',
                    'value'     => $args['to'],
                    'compare'   => '<=',
                    'type'      => 'NUMERIC'
                )
            );
            break;
        case 'within' :
            $meta_query = array( 
                'relation' => 'AND',
                array(
                    'key'       => 'event_starting_date',
                    'value'     => $args['from'],
                    'compare'   => '>=',
                    'type'      => 'NUMERIC'
                ),
                array(
                    'key'       => 'event_starting_date',
                    'value'     => $args['to'],
                    'compare'   => '<=',
                    'type'      => 'NUMERIC'
                )
            );
            break;
        case 'day' :
            $meta_query = array( 
                'relation' => 'AND',
                array(
                    'key'       => 'event_starting_date',
                    'value'     => $args['date'],
                    'compare'   => '<=',
                    'type'      => 'NUMERIC'
                ),
                array(
                    'key'       => 'event_ending_date',
                    'value'     => $args['date'],
                    'compare'   => '>=',
                    'type'      => 'NUMERIC'
                )
            );
            break;
        default :
            $meta_query = array();
    }

    remove_all_filters('posts_orderby');

    $events = new WP_Query( array(
        'post_type'         => 'event',
        'posts_per_page'    => $args['limit'],
        'paged'             => $args['page'],
        'order'             => $args['order'],
        'orderby'           => $args['orderby'],
        'meta_key'          => $args['meta_key'],
        'meta_query'        => $meta_query
    ));

    if ( ! empty( $events->posts ) ) {
        $counter = 0;

        foreach ( $events->posts as $event ) {

            $events->posts[$counter]->permalink     = get_permalink( $event->ID );
            $events->posts[$counter]->starting      = get_field( 'event_starting_date', $event->ID );
            $events->posts[$counter]->ending        = get_field( 'event_ending_date', $event->ID );

            $counter++;
        }        
    }

    return $events;
}

function acf_events_datestamp( $timestamp ) {

    if ( is_numeric( $timestamp ) && strlen( (string)$timestamp ) == 10 ) {
        // $timestamp is an actual timestamp... so do not do anything
    } else {
        $timestamp = strtotime( $timestamp );
    }

    return date( 'Ymd', $timestamp );
}

function acf_events_date_format( $start, $end = null, $divider = ' — ' ) {

    if ( null === $end || empty( $end ) || ! $end ) {
        return date_i18n( get_option( 'date_format' ), strtotime( $start ) );
    }

    $start_timestamp    = strtotime( $start );
    $end_timestamp      = strtotime( $end );

    if ( $start_timestamp == $end_timestamp ) {
        return date_i18n( get_option( 'date_format' ), $start_timestamp );
    }

    $s = array(
        'Y' => date( 'Y', $start_timestamp ),
        'M' => date( 'F', $start_timestamp ),
        'D' => date( 'jS', $start_timestamp )
    );

    $e = array(
        'Y' => date( 'Y', $end_timestamp ),
        'M' => date( 'F', $end_timestamp ),
        'D' => date( 'jS', $end_timestamp )
    );

    if ( $s['Y'] != $e['Y'] ) {
        return date_i18n( get_option( 'date_format' ), $start_timestamp ) . $divider . date_i18n( get_option( 'date_format' ), $end_timestamp );
    } else {
        if ( $s['M'] != $e['M'] ) {
            return $s['M'] . ' ' . $s['D'] . $divider . $e['M'] . ' ' . $e['D'] . ', ' . $s['Y'];
        } else {
            return $s['M'] . ' ' . $s['D'] . $divider . $e['D'] . ', ' . $s['Y'];
        }
    }
}

function get_events_calendar( $calmonth = null, $calyear = null ) {
        
    $time               = time();
    $year               = $calyear !== null ? date( 'Y', $time ) : $calyear;
    $month              = $calmonth !== null ? date( 'm', $time ) : $calmonth;
    $day_name_length    = 7;
    $month_href         = NULL;
    $first_day          = 0; 
    $today              = date( 'Ymd', $time );

    if ( isset( $_GET['caldate'] ) ) {
        $year   = substr( $_GET['caldate'], 0, 4 );
        $month  = substr( $_GET['caldate'], 4, 2 );
    }
    
    if ( (int)$month == 1 ) {
        $next_month = $month + 1;
        $prev_month = 12;
        $prev_year  = $year - 1;
        $next_year  = $year;
    } else if ( (int)$month == 12 ) {
        $next_month = 1;
        $prev_month = $month - 1;
        $prev_year  = $year;
        $next_year  = $year + 1;
    } else {
        $next_month = $month + 1;
        $prev_month = $month - 1;
        $prev_year  = $year;
        $next_year  = $year;
    }
    
    if ( $prev_month < 10 ) (string)$prev_month = '0' . $prev_month;
    if ( $next_month < 10 ) (string)$next_month = '0' . $next_month;
    
    $pn = array (
        '&laquo;' => $prev_year . $prev_month,
        '&raquo;' => $next_year . $next_month
    );
    
    $first_of_month = gmmktime( 0, 0, 0, (int)$month, 1, (int)$year );
    $days_in_month  = gmdate( 't', $first_of_month );
    $end_of_month   = gmmktime( 0, 0, 0, (int)$month, $days_in_month, (int)$year );
    $day_names      = array();

    for ( $n = 0, $t = ( 3 + $first_day ) * 86400; $n < 7; $n++, $t += 86400 ) {
        $day_names[$n] = ucfirst( gmstrftime( '%A', $t ) );
    }

    list( $month, $year, $month_name, $weekday ) = explode( ',', gmstrftime( '%m, %Y, %B, %w', $first_of_month ) );
    $weekday = ( $weekday + 7 - $first_day ) % 7;
    $title   = htmlentities( ucfirst($month_name) ) . $year;

    // Begin calendar
    @list( $p, $pl ) = each($pn);
    @list( $n, $nl ) = each($pn);

    if ( $p ) {        
        $p = ( $pl ? '<a href="' . add_query_arg( 'caldate', $pl ) . '" class="event-calendar-prev">' . $p . '</a>' : $p ) . PHP_EOL;
    }

    if( $n ) {        
        $n = ( $nl ? '<a href="' . add_query_arg( 'caldate', $nl ) . '" class="event-calendar-next">' . $n . '</a>' : $n ) . PHP_EOL;
    }

    $calendar = '<div id="event-calendar">' . PHP_EOL . '<table>' . PHP_EOL . '<caption class="event-calendar-caption">' . $p . ($month_href ? '<a href="' . htmlspecialchars($month_href) . '">' . $title . '</a>' : $title) . $n . '</caption>' . PHP_EOL . '<tr class="event-calendar-header">' . PHP_EOL;

    if ( $day_name_length ) {        
        foreach ( $day_names as $d ) {
            $calendar  .= '<th abbr="' . htmlentities($d) . '" width="14.285%">' . htmlentities( $day_name_length < 4 ? substr( $d, 0, $day_name_length ) : $d ) . '</th>' . PHP_EOL;
        }
        $calendar  .= '</tr>' . PHP_EOL . '<tr class="event-calendar-row">' . PHP_EOL;
    }

    if ( $weekday > 0 ) {
        for ( $i = 0; $i < $weekday; $i++ ) {
            $calendar  .= '<td>&nbsp;</td>' . PHP_EOL;
        }
    }

    $events = get_events(
        array(
            'type' => 'within',
            'from' => acf_events_datestamp($first_of_month),
            'to' => acf_events_datestamp($end_of_month)
        )
    );

    for ( $day = 1, $days_in_month; $day <= $days_in_month; $day++, $weekday++ ) {
        $event_list = '';
        $curdate = $year . $month . ( $day < 10 ? '0' . $day : $day );

        foreach ( $events->posts as $e ) {
            if ( (int)$e->starting <= (int)$curdate && (int)$e->ending >= (int)$curdate ) {
                $event_list .= '<a href="' . $e->permalink . '" title="' . wp_trim_words( strip_tags( $e->post_content ), 20 ). '" class="event-calendar-event">' . $e->post_title . '</a>' . PHP_EOL;
            }
        }
        
        if ( $weekday == 7 ) {
            $weekday   = 0;
            $calendar  .= '</tr>' . PHP_EOL . '<tr class="event-calendar-row">' . PHP_EOL;
        }
        
        $calendar .= '<td' . ( $today == $curdate ? ' id="today"' : '' ) . '>' . PHP_EOL . '<span class="event-calendar-day">' . $day . '</span>' . PHP_EOL . $event_list . '</td>' . PHP_EOL;
    }

    if ( $weekday != 7 ) {
        $calendar .= '<td id="event-calendar-empty" colspan="' . (7-$weekday) . '">&nbsp;</td>' . PHP_EOL;
    }

    return $calendar . '</tr>' . PHP_EOL . '</table>' . PHP_EOL . '</div>' . PHP_EOL;
}

?>