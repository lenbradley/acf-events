jQuery(document).ready( function($) {
    $(window).load( function() {
        var starting_date   = $('#acf-event_starting_date input[type="text"]');
        var ending_date     = $('#acf-event_ending_date input[type="text"]');

        if ( starting_date.length && ending_date.length ) {
           $(document).on( 'change', starting_date, function() {
                var starting    = starting_date.datepicker('getDate');
                var ending      = ending_date.datepicker('getDate');

                if ( ! ending || starting > ending ) {
                    ending_date.datepicker( 'setDate', starting );
                }
           });
        }
    });    
});