<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function acf_events_admin_columns() {
    $columns = array(
        'cb' => '<input type="checkbox" />',        
        'title' => __( 'Event Title' ),
        'author' => __( 'Author' ),
        'event_starting_date' => __( 'Starting Date' ),
        'event_ending_date' => __( 'Ending Date' ),
        'date' => __( 'Created' )
    );

    return $columns;
}
add_filter( 'manage_edit-event_columns', 'acf_events_admin_columns' );

function acf_events_sortable_columns( $columns ) {
    $columns['event_starting_date'] = 'event_starting_date';
    $columns['event_ending_date'] = 'event_ending_date';
    return $columns;
}
add_filter( 'manage_edit-event_sortable_columns', 'acf_events_sortable_columns' );

function acf_add_event_date_to_post_content( $content ) {
    if ( is_single() && get_post_type() == 'event' ) {
        $event_meta_content = '<dl id="event-meta"><dt>Event Date:</dt><dd>' . acf_events_date_format( get_field('event_starting_date'), get_field('event_ending_date') ) . '</dd></dl>' . PHP_EOL;
        return $event_meta_content . $content;
    } else {
        return $content;
    }
}
add_filter( 'the_content', 'acf_add_event_date_to_post_content' );

?>